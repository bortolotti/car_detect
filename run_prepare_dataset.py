# ==================================================================================================
# Gerar o arquivo csv que contém as marcações dos veículos nas imagens
# Autor: Carlos V. Bortolotti
# ==================================================================================================

from matplotlib import pyplot as plt
import numpy as np
import cv2
import pandas as pd
import os
from PIL import Image
import io

DATASET_BASE_DIRECTORY = "./dataset/"
DATASET_IMAGE_DIRECTORY = "./dataset/images/"
DATASET_LABEL_DIRECTORY = "./dataset/labels/"

# ================================================================================
# get_file_size -> Retornar o tamanho do arquivo.
# ================================================================================

def get_file_size(file_name : str):
    with open(os.path.join(DATASET_IMAGE_DIRECTORY, file_name), 'rb') as fid:
        encoded = fid.read()
    encoded_io = io.BytesIO(encoded)
    image = Image.open(encoded_io)
    return image.size

# ================================================================================
# get_line_info -> Transformar uma linha do arquivo texto para o objeto padrão.
# ================================================================================

def get_line_info(file_name : str, line : str) -> dict:
    # Saída ->
    # filename
    # width
    # height
    # class
    # xmin
    # ymin
    # xmax
    # ymax    
    if line is None:
        return None
    
    columns = line.split(' ')
    
    width_file, height_file = get_file_size(file_name)
    
    if len(columns) == 14:

        frame_size = { 'width' : width_file, 'height' : height_file }

        return { 
                "filename" : file_name,
                "class" : int(columns[3]) + 1,
                "width_file" : width_file,
                "height_file" : height_file,
                "xmin" : np.min([np.min([int(columns[6]), int(frame_size['width'])]), np.min([int(columns[7]), int(frame_size['width'])]), np.min([int(columns[8]), int(frame_size['width'])]), np.min([int(columns[9]), int(frame_size['width'])])]),
                "ymin" : np.min([np.min([int(columns[10]), int(frame_size['height'])]), np.min([int(columns[11]), int(frame_size['height'])]), np.min([int(columns[12]), int(frame_size['height'])]), np.min([int(columns[13]), int(frame_size['height'])])]),
                "xmax" : np.max([np.min([int(columns[6]), int(frame_size['width'])]), np.min([int(columns[7]), int(frame_size['width'])]), np.min([int(columns[8]), int(frame_size['width'])]), np.min([int(columns[9]), int(frame_size['width'])])]),
                "ymax" : np.max([np.min([int(columns[10]), int(frame_size['height'])]), np.min([int(columns[11]), int(frame_size['height'])]), np.min([int(columns[12]), int(frame_size['height'])]), np.min([int(columns[13]), int(frame_size['height'])])]),
                "className" : columns[0],
                "format" : "14"
            }

    elif len(columns) == 5:
        x = float(columns[1])
        y = float(columns[2])
        width = float(columns[3])
        height = float(columns[4])
        xmin, ymin = int((x * width_file) - ((width * width_file)/2)), int((y * height_file) - ((height * height_file))/2)
        xmax, ymax = int((x * width_file) + ((width * width_file)/2)), int((y * height_file) + ((height * height_file))/2)
        
        return { 
                "filename" : file_name,
                "class" : int(columns[0]) + 1,
                "x" : x,
                "y" : y,
                "width" : width,
                "height" : height,
                "width_file" : width_file,
                "height_file" : height_file,        
                "xmin" : xmin,
                "ymin" : ymin,
                "xmax" : xmax,
                "ymax" : ymax,
                "className" : ["car", "truck", "bus", "minibus", "cyclist"][int(columns[0])],
                "format" : 5
            }

    else:
        return None

# ================================================================================
# load_label_file -> Carregar o arquivo que contém as marcações
# ================================================================================

def load_label_file(label_directory : str, label_file_name : str, image_file_name : str):
    image_files = []
    full_file_path = os.path.join(label_directory, label_file_name)    
    with open(full_file_path, 'r') as file:        
        for line in file:
            info = get_line_info(image_file_name, line)
            if not info is None:
                image_files.append(info)
    return image_files

# ================================================================================
# get_markup_data -> Carregar as marcações dos arquivos
# ================================================================================

def get_markup_data():
    images = os.listdir(DATASET_IMAGE_DIRECTORY)
    labels = os.listdir(DATASET_LABEL_DIRECTORY)
    file_marks = []
    
    for label_file in labels:
        print(label_file)
        file_parts = label_file.split('.')

        if file_parts is None or len(file_parts) == 0:
            continue

        file_name = file_parts[0]
        find_file_images = list(filter(lambda x: x == "{0}.png".format(file_name) or x == "{0}.jpg".format(file_name), images))

        if find_file_images is None or len(find_file_images) == 0:
            continue

        label_file_data = load_label_file(
            DATASET_LABEL_DIRECTORY, 
            label_file, 
            find_file_images[0])

        if label_file_data is None or len(label_file_data) == 0:
            continue

        for _ in label_file_data:
            file_marks.append(_)

    return file_marks

# Carregar os arquivos de marcação
data = get_markup_data()
df = pd.DataFrame(data)
df = pd.DataFrame(data)
df['height_object'] = df.ymax - df.ymin
df['width_object'] = df.xmax - df.xmin
df['area_object'] = df.width_object * df.height_object
df = df[(df.area_object > 600) | (df.format == 5)]
df.to_csv(os.path.join(DATASET_BASE_DIRECTORY, 'dataset.csv'), header=True, index=False)

# Randomizar o nome dos arquivos e criar dataframe para separar a base de teste e treinamento por arquivo
df_filename_split = pd.DataFrame({ 'filename' : df.filename.unique()})
df_filename_split = df_filename_split.sample(frac=1)
test_records = int(df_filename_split.count() * 0.2)

# Criar dataset de treinamento / testes
df_train = df.join(pd.DataFrame(df_filename_split[test_records:]).set_index('filename'), on='filename', how='inner')
df_train.to_csv(os.path.join(DATASET_BASE_DIRECTORY, 'train.csv'), header=True, index=False)

df_test = df.join(pd.DataFrame(df_filename_split[:test_records]).set_index('filename'), on='filename', how='inner')
df_test.to_csv(os.path.join(DATASET_BASE_DIRECTORY, 'test.csv'), header=True, index=False)