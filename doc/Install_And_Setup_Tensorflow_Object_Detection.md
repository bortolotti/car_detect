https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10#1-install-anaconda-cuda-and-cudnn

1. Baixar e instalar:
    Python
    TensorFlow
    Tensorboard
    Protobuf v3.4 or above

    # For CPU 
    pip install tensorflow 
    # For GPU 
    pip install tensorflow-gpu

    pip install --user Cython 
    pip install --user contextlib2 
    pip install --user pillow 
    pip install --user lxml 
    pip install --user jupyter
    pip install --user matplotlib

    Protobuf -> https://github.com/google/protobuf/releases
    Modelos -> https://github.com/tensorflow/models

2. Criar uma pasta chamada Tensorflow.
    Extrair os modelos para dentro de uma sub-pasta chamada Tensorflow\models
    Extrair o protobuf para dentro de uma sub-pasta chamada Tensorflow\protobuf

3. Na linha de comando, entrar na pasta "Tensorflow\models\research" Gerar a camada python dos protos encontrados dentro do modelo:
    protoc --python_out=. .\object_detection\protos\anchor_generator.proto .\object_detection\protos\argmax_matcher.proto .\object_detection\protos\bipartite_matcher.proto .\object_detection\protos\box_coder.proto .\object_detection\protos\box_predictor.proto .\object_detection\protos\eval.proto .\object_detection\protos\faster_rcnn.proto .\object_detection\protos\faster_rcnn_box_coder.proto .\object_detection\protos\grid_anchor_generator.proto .\object_detection\protos\hyperparams.proto .\object_detection\protos\image_resizer.proto .\object_detection\protos\input_reader.proto .\object_detection\protos\losses.proto .\object_detection\protos\matcher.proto .\object_detection\protos\mean_stddev_box_coder.proto .\object_detection\protos\model.proto .\object_detection\protos\optimizer.proto .\object_detection\protos\pipeline.proto .\object_detection\protos\post_processing.proto .\object_detection\protos\preprocessor.proto .\object_detection\protos\region_similarity_calculator.proto .\object_detection\protos\square_box_coder.proto .\object_detection\protos\ssd.proto .\object_detection\protos\ssd_anchor_generator.proto .\object_detection\protos\string_int_label_map.proto .\object_detection\protos\train.proto .\object_detection\protos\keypoint_box_coder.proto .\object_detection\protos\multiscale_anchor_generator.proto .\object_detection\protos\graph_rewriter.proto .\object_detection\protos\calibration.proto .\object_detection\protos\flexible_grid_anchor_generator.proto

4. Em seguida executar:
    python setup.py build
    python setup.py install

5.Em seguida, entrar na pasta "Tensorflow\models\research\object_detection" e digitar:
    jupyter notebook object_detection_tutorial.ipnby

6. Executar o notebook para verificar se tudo ok.

7. Classificar e identificar as imagens (labels)

8. Executar o script generate_tfrecord.py deste tutorial:
    python generate_tfrecord.py --csv_input=images\train_labels.csv --image_dir=images\train --output_path=train.record
    python generate_tfrecord.py --csv_input=images\test_labels.csv --image_dir=images\test --output_path=test.record

9. Criar o arquivo label map -> labelmap.pbtxt

    item {
    id: 1
    name: 'nine'
    }

    item {
    id: 2
    name: 'ten'
    }

    item {
    id: 3
    name: 'jack'
    }

    item {
    id: 4
    name: 'queen'
    }

    item {
    id: 5
    name: 'king'
    }

    item {
    id: 6
    name: 'ace'
    }

10. Copiar o arquivo .\models\research\object_detection\samples\configs\faster_rcnn_inception_v2_pets.config para \object_detection\training.

11.
python .\legacy\train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_inception_v2_pets.config

python export_inference_graph.py --input_type image_tensor --pipeline_config_path training/faster_rcnn_inception_v2_pets.config --trained_checkpoint_prefix training/model.ckpt-187377 --output_directory inference_graph

tensorboard --logdir=training

