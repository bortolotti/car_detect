import tensorflow as tf
import os
import numpy as np

from config.settings import Settings
from utils import label_map_util
from utils import visualization_utils as vis_util

class ModelNetwork:
    """ Classe responsável pela carga da rede treinada em tensorflow """

    def __init__(self):
        """ Construtor padrão """
        self.__label_map = None
        self.__categories = None
        self.__category_index = None
        self.__detection_graph = None
        self.__sess = None
        self.__image_tensor = None
        self.__detection_boxes = None
        self.__detection_scores = None
        self.__detection_classes = None
        self.__num_detections = None
        self.__load_model()

    def __new__(cls, *args, **kwargs):
        """ Singleton """
        if not hasattr(cls, '_instance'):
            cls._instance = super(ModelNetwork, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __load_model(self):
        """ Carregar a rede treinada """
        settings = Settings()
        network_checkpoint_file_path = settings.get_network_checkpoint_file_path
        network_label_file_path = settings.get_network_label_file_path

        if network_checkpoint_file_path is None:
            raise Exception("Model checkpoint file path is required")

        if network_label_file_path is None:
            raise Exception("Model label file path is required")

        if not os.path.isfile(network_checkpoint_file_path):
            raise Exception("Model checkpoint file %s not found" % network_checkpoint_file_path)

        if not os.path.isfile(network_label_file_path):
            raise Exception("Model checkpoint file %s not found" % network_label_file_path)

        if settings.get_network_number_classes <= 0:
            raise Exception("Invalid number of classes")

        label_map = label_map_util.load_labelmap(network_label_file_path)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=settings.get_network_number_classes, use_display_name=True)
        category_index = label_map_util.create_category_index(categories)

        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(network_checkpoint_file_path, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            sess = tf.Session(graph=detection_graph)

        # Carregar os parâmetros da rede treinada
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        self.__label_map = label_map
        self.__categories = categories
        self.__category_index = category_index
        self.__detection_graph = detection_graph
        self.__image_tensor = image_tensor
        self.__detection_boxes = detection_boxes
        self.__detection_scores = detection_scores
        self.__detection_classes = detection_classes
        self.__num_detections = num_detections
        self.__sess = sess
        
        return
       
    def detect(self, image, drawDetectOnImage : bool = False) -> tuple:
        """ Efetuar a detecção e desenhar o resultado na imagem """
        settings = Settings()

        frame_expanded = np.expand_dims(image, axis=0)

        (boxes, scores, classes, num) = self.__sess.run(
            [self.__detection_boxes, self.__detection_scores, self.__detection_classes, self.__num_detections],
            feed_dict={self.__image_tensor: frame_expanded})

        # Desenhar o resultado
        if drawDetectOnImage:
            vis_util.visualize_boxes_and_labels_on_image_array(
                image,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                self.__category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
                min_score_thresh=settings.get_network_min_score_thresh)

        return (boxes, scores, classes, num)
