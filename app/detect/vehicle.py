import threading
import cv2
import numpy as np

from detect.util import CalcProcess
from config.settings import Settings

class VehiclePosition:
    """ Posição do veículo """

    def __init__(self, frame_number : int, xmin : int, ymin : int, xmax : int, ymax : int):
        """ Construtor padrão da classe """
        self.frame_number = frame_number
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.is_valid = True

    def get_tuple(self):
        """ Retornar a tupla que contém a informação do veículo """
        return (self.xmin, self.ymin, self.xmax, self.ymax)
        
class Vehicle:
    """ Classe responsável pela implementação da estrutura e identificação de um veículo """

    # Atributos estáticos
    __calc_process = CalcProcess()
    __locker = threading.Lock()
    __counter = 0

    # Construtores
    def __init__(self):
        """ Construtor padrão """
        try:
            Vehicle.__locker.acquire()
            Vehicle.__counter = Vehicle.__counter + 1
            self.__id = Vehicle.__counter
        finally:
            Vehicle.__locker.release()
        self.__tracks = list()
        self.__collision_marks = list()
        return

    # Propriedades
    @property
    def id(self):
        """"Retornar o ID do objeto """
        return self.__id

    @property
    def vehicle_class(self):
        """ Retornar o código da classe da detecção """
        return self.__vehicle_class

    @vehicle_class.setter 
    def vehicle_class(self, value : int):
        """ Retornar o código da classe da detecção """
        self.__vehicle_class = value

    @property
    def tracks(self) -> list:
        """ Retornar a lista de tracks """
        return self.__tracks

    @property
    def collision_marks(self) -> list:
        """ Retornar a lista de colisões das marcações """
        return self.__collision_marks

    # Métodos
    def check_collision_mark(self, label : str):
        """ Retornar verdadeiro, caso o veículo já tenha colidido com a linha """
        r = True
        try:
            self.__collision_marks.index(label)
        except ValueError:
            r = False
        return r

    def register_collision_mark(self, label : str):
        """ Registrar a colisão com uma marcação até duas no máximo (entrada/saída) """
        try:
            self.__collision_marks.index(label)
        except ValueError:
            self.__collision_marks.append(label)

    def register_tracker(self, frame_number : int, xmin : int, ymin : int, xmax : int, ymax : int):
        """ Registrar a posição do veículo """
        for track in self.__tracks:
            track.is_valid = False
        self.__tracks.append(VehiclePosition(frame_number, xmin, ymin, xmax, ymax))

    def get_last_position(self) -> VehiclePosition:
        """ Retornar a última posição do tracker do veículo """
        r = None
        if not self.__tracks is None and len(self.__tracks) > 0:
            r = self.__tracks[-1]
        return r

    def check_collision(self, xmin, ymin, xmax, ymax):
        """ Verificar se este veículo colide com este objeto """
        vehicle_position = self.get_last_position()
        if vehicle_position is None:
            return False
        return Vehicle.__calc_process.check_collision((xmin, ymin, xmax, ymax), vehicle_position.get_tuple())

    def calculate_collision_area(self, w, h, xmin, ymin, xmax, ymax) -> int:
        """ Calcular a área de colisão para determinar qual o objeto mais provável """
        vehicle_position = self.get_last_position()
        if vehicle_position is None:
            return 0
        r_vehicle = (tuple(int(v * (w if i % 2 == 0 else h)) for i, v in enumerate(vehicle_position.get_tuple())))
        r_object = (tuple(int(v * (w if i % 2 == 0 else h)) for i, v in enumerate((xmin, ymin, xmax, ymax))))
        return Vehicle.__calc_process.calculate_intersection_area(r_object, r_vehicle)

    def average_distance_between_frame(self) -> float:
        """ Calcular a distância média entre os frames do objeto """
        values = []
        for i, v in enumerate(self.__tracks):
            if (i > 0):
                vp = self.__tracks[i - 1]
                p1 = Vehicle.__calc_process.get_centroid(vp.xmin, vp.ymin, vp.xmax, vp.ymax)
                p2 = Vehicle.__calc_process.get_centroid(v.xmin, v.ymin, v.xmax, v.ymax)
                r = Vehicle.__calc_process.calculate_eucledian_distance(p1[0], p1[1], p2[0], p2[1])
                if r != 0:
                    values.append(r)
        return 0 if len(values) == 0 else np.mean(values)

    def get_frame_count(self):
        return len(self.__tracks)

    def average_size_between_frame(self) -> float:
        values = []
        for v in self.__tracks:
            values.append((v.xmax - v.xmin) * (v.ymax - v.ymin))
        return np.mean(values)    

    def is_valid(self, check_mark_collision = False) -> bool:
        """ Retornar verdadeiro se o veículo possui algum rastro válido """
        return (not check_mark_collision or len(self.collision_marks) <= 1) and sum([_.frame_number for _ in filter(lambda x: x.is_valid, self.__tracks)]) > 0

class VehicleManager:
    """ Classe responsável pela criação e o gerenciamento dos veículos """

    __settings = Settings()
    __calc_process = CalcProcess()

    def __new__(cls, *args, **kwargs):
        """ Singleton """
        if not hasattr(cls, '_instance'):
            cls._instance = super(VehicleManager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        """ Construtor padrão """
        self.__vehicles = list()
    
    def get_vehicles(self):
        """ Retornar a lista de veículos """
        return self.__vehicles

    def track_vehicles(self, frame_number, frame, boxes, scores, classes):
        """ Efetuar o rastreamento dos veículos """
        h, w, _ = frame.shape
        min_score_thresh = VehicleManager.__settings.get_network_min_score_thresh

        tracked_boxes = []
        
        # 1. Identificar colisões
        for i in range(boxes[0].shape[0]):
            ymin, xmin, ymax, xmax = boxes[0][i]
            vehicle_class = classes[0][i]
            score = scores[0][i]

            if score < min_score_thresh:
                continue
                
            possible_vehicles = list(filter(
                        lambda x: x.is_valid() and not x.get_last_position() is None and x.get_last_position().is_valid and x.check_collision(xmin, ymin, xmax, ymax), 
                        self.__vehicles))           

            possible_vehicles.sort(
                key=lambda x: x.calculate_collision_area(w, h, xmin, ymin, xmax, ymax),
                reverse=True)                

            vehicle = None

            for pi, pv in enumerate(possible_vehicles):

                if pi == 0:
                    vehicle = pv
                else:
                    last_position = pv.get_last_position()
                    if not last_position is None:
                        last_position.is_valid = False

            if vehicle is None:
                continue

            vehicle.vehicle_class = vehicle_class
            vehicle.register_tracker(frame_number, xmin, ymin, xmax, ymax)
            tracked_boxes.append(i)


        # 2. Caso não encontre colisão, então calcular a distância euclediana entre os centróides e tentar encontrar o rastro possível
        try:
            average_frame = int(sum([_.get_frame_count() for _ in self.__vehicles]) / len(self.__vehicles))
        except ZeroDivisionError:
            average_frame = 0

        for i in range(boxes[0].shape[0]):

            try:
                was_tracked = tracked_boxes.index(i) >= 0
            except ValueError:
                was_tracked = False

            if was_tracked:
                continue                

            ymin, xmin, ymax, xmax = boxes[0][i]
            vehicle_class = classes[0][i]
            score = scores[0][i]

            if score < min_score_thresh:
                continue

            vehicle = None

            # Caso não encontre o veículo, tentar localizar o rastro de veículo ativo mais próximo
            # Através do cálculo da distância euclediana. 
            # Também calcular a média e o erro padrão das distâncias entre um frame e outro, expurgando os registros
            # fora do intervalo de confiança de 0.95 da curva de gauss

            valid_vehicles = list(
                filter(
                    lambda x: x.is_valid(True) and not x.get_last_position() is None and x.get_last_position().is_valid and x.get_last_position().frame_number < frame_number and x.get_last_position().frame_number > (frame_number - average_frame),
                    self.__vehicles))

            box_centroid = VehicleManager.__calc_process.get_centroid(xmin, ymin, xmax, ymax)
            box_size = ((xmax - xmin) * (ymax - ymin))
            closest_possibles_vehicles = []

            for vv in valid_vehicles:
                vv_position = vv.get_last_position()
                vv_area = vv.average_size_between_frame()
                average_distance = vv.average_distance_between_frame()
                projector_distance = ((frame_number - vv_position.frame_number) * average_distance)
                vv_centroid = VehicleManager.__calc_process.get_centroid(vv_position.xmin, vv_position.ymin, vv_position.xmax, vv_position.ymax)
                calculate_distance = VehicleManager.__calc_process.calculate_eucledian_distance(box_centroid[0], box_centroid[1], vv_centroid[0], vv_centroid[1])

                if (box_size >= (vv_area * .95) or box_size <= (vv_area * 1.05)) and (calculate_distance >= (projector_distance * .95) or calculate_distance <= (projector_distance * 1.05)):
                    closest_possibles_vehicles.append({ "vehicle" : vv, "distance" : calculate_distance })

            if len(closest_possibles_vehicles) > 0:
                closest_possibles_vehicles.sort(key=lambda x: x['distance'])
                vehicle = closest_possibles_vehicles[0]['vehicle']

            if vehicle is None:
                continue

            vehicle.vehicle_class = vehicle_class
            vehicle.register_tracker(frame_number, xmin, ymin, xmax, ymax)
            tracked_boxes.append(i)            


        # 3. Criar o veículo
        for i in range(boxes[0].shape[0]):

            try:
                was_tracked = tracked_boxes.index(i) >= 0
            except ValueError:
                was_tracked = False

            if was_tracked:
                continue                

            ymin, xmin, ymax, xmax = boxes[0][i]
            vehicle_class = classes[0][i]
            score = scores[0][i]

            if score < min_score_thresh:
                continue
                
            vehicle = Vehicle()
            self.__vehicles.append(vehicle)
            vehicle.vehicle_class = vehicle_class
            vehicle.register_tracker(frame_number, xmin, ymin, xmax, ymax)
            tracked_boxes.append(i)

        return

    def get_vehicle_by_position(self, xmin, ymin, xmax, ymax):
        """ Retornar o veículo de uma determinada posição """
        r = None

        for vehicle in filter(lambda x: x.is_valid(), self.__vehicles):

            vehicle_position = vehicle.get_last_position()            
            
            if vehicle_position is None or not vehicle_position.is_valid:
                continue

            if ymin == vehicle_position.ymin and xmin == vehicle_position.xmin and ymax == vehicle_position.ymax and xmax == vehicle_position.xmax:
               r = vehicle
               break

        return r

    def draw(self, image, boxes):
        """ Desenhar os IDs do veículos e tracking """
        h, w, _ = image.shape

        for vehicle in filter(lambda x: x.is_valid(), self.__vehicles):          
            
            vehicle_position = vehicle.get_last_position()
            
            if vehicle_position is None or not vehicle_position.is_valid:
                continue

            box_found = sum([1 
                             for ymin, xmin, ymax, xmax in boxes[0]
                             if ymin == vehicle_position.ymin and xmin == vehicle_position.xmin and ymax == vehicle_position.ymax and xmax == vehicle_position.xmax]) > 0

            if not box_found:
                continue

            text = "ID:%i" % (vehicle.id)

            vehicle_centroid = VehicleManager.__calc_process.get_centroid(
                                    vehicle_position.xmin, 
                                    vehicle_position.ymin, 
                                    vehicle_position.xmax, 
                                    vehicle_position.ymax)

            rectangle_position = (vehicle_position.xmax, vehicle_position.ymin - .15, vehicle_position.xmax + .06, vehicle_position.ymin - .10)

            rectangle_centroid = VehicleManager.__calc_process.get_centroid(
                                    rectangle_position[0], 
                                    rectangle_position[1], 
                                    rectangle_position[2], 
                                    rectangle_position[3])

            #cv2.rectangle(image, (int(rectangle_position[0] * w), int(rectangle_position[1] * h)), (int(rectangle_position[2] * w), int(rectangle_position[3] * h)), (0, 0, 0), -1)

            cv2.line(
                image,
                (int(rectangle_centroid[0] * w), int(rectangle_centroid[1] * h)),
                (int(vehicle_centroid[0] * w), int(vehicle_centroid[1] * h)),
                (0, 0, 0),
                1)

            cv2.putText(
                image,
                text, 
                (int((rectangle_centroid[0]) * w), int((rectangle_centroid[1]) * h)),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.35,
                (0, 0, 0),
                1,
                cv2.LINE_AA)

            # last_track_centroid = None
            # for track in vehicle.tracks:
            #     track_centroid = VehicleManager.__calc_process.get_centroid(
            #                             track.xmin, 
            #                             track.ymin, 
            #                             track.xmax, 
            #                             track.ymax)
            #     if last_track_centroid is None:
            #         last_track_centroid = track_centroid
            #     cv2.line(
            #         image,
            #         (int(track_centroid[0] * w), int(track_centroid[1] * h)),
            #         (int(last_track_centroid[0] * w), int(last_track_centroid[1] * h)),
            #         (0, 0, 0),
            #         1)
            #     last_track_centroid = track_centroid
        
        return