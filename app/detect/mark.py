import cv2
import numpy as np

from config.settings import Settings
from detect.util import CalcProcess
from detect.vehicle import Vehicle
from detect.vehicle import VehicleManager

class LineMark:
    """ Classe que contém as informações da linha """

    def __init__(self, color : tuple, startPosition : tuple, endPosition : tuple):
        """ Construtor padrão """
        self.__color = color
        self.__startPosition = startPosition
        self.__endPosition = endPosition

    @property
    def color(self) -> tuple:
        """ Retorna a tupla que contém as informações da cor da linha """
        return self.__color

    @property
    def start_position(self) -> tuple:
        """ Retorna a tupla que contém as informações da posição inicial da linha """
        return self.__startPosition

    @property
    def end_position(self) -> tuple:
        """ Retorna a tupla que contém as informações da posição final da linha """
        return self.__endPosition

    @property
    def x1(self) -> int:
        return self.start_position[0]

    @property
    def y1(self) -> int:
        return self.start_position[1]

    @property
    def x2(self) -> int:
        return self.end_position[0]

    @property
    def y2(self) -> int:
        return self.end_position[1]

    @property
    def line_slope(self) -> float:
        """ Retornar a inclinação da linha """
        x1, y1 = self.start_position
        x2, y2 = self.end_position
        try:
            m = (x2 - x1) / (y2 - y1)
        except ZeroDivisionError:
            m = 0
        return m

class Mark:
    """ Classe que contém as informações de marcação da linha """

    __vehicle_manager = VehicleManager()

    def __init__(self, label : str, description : str, color : tuple, startPosition : tuple, endPosition : tuple):
        """ Construtor padrão da classe """
        self.__calc_process = CalcProcess()
        self.__label = label
        self.__description = description
        self.__line_definition = LineMark(color, startPosition, endPosition)
        self.__collision_counter = 0
        self.__last_collision = (0, 0, 0, 0)

    def get_line_slope(self) -> float:
        """ Retornar a inclinação da linha """
        return self.__line_definition.line_slope

    @property
    def label(self):
        """ Retornar a label da marcação """
        return self.__label

    @property
    def description(self):
        """ Retornar a descrição da marcação """
        return self.__description

    @property
    def collision_counter(self):
        """ Retornar o número de colisões com a marcação """
        return int(self.__collision_counter)

    def check_collision_object(self, image, boxes, scores, classes):
        """ Verificar se existe a colisão dos objetos """
        if boxes is None or scores is None or classes is None:
            return

        settings = Settings()
        min_score_thresh = settings.get_network_min_score_thresh
        h, w, _ = image.shape
        line_xmin, line_ymin, line_xmax, line_ymax = (self.__line_definition.x1 / w, self.__line_definition.y1 / h, self.__line_definition.x2 / w, self.__line_definition.y2 / h)
        is_vertical_mark = (self.__line_definition.y2 - self.__line_definition.y1) > (self.__line_definition.x2 - self.__line_definition.x1) 

        for i in range(boxes[0].shape[0]):

            ymin, xmin, ymax, xmax = boxes[0][i]
            #vehicle_class = classes[0][i]
            score = scores[0][i]

            # Tolerância de 50% do tamanho do objeto
            tolerance_x = (xmax - xmin) / 2
            tolerance_y = (ymax - ymin) / 2

            if score < min_score_thresh:
                continue

            box_centroid = self.__calc_process.get_centroid(xmin, ymin, xmax, ymax)
            is_collision = False

            if (is_vertical_mark):
                is_collision = (((box_centroid[0] >= line_xmin - tolerance_x) and (box_centroid[0] <= line_xmax + tolerance_x)) and 
                                 (box_centroid[1] >= line_ymin and box_centroid[1] <= line_ymax))
            else:
                is_collision = (((box_centroid[1] >= line_ymin - tolerance_y) and (box_centroid[1] <= line_ymax + tolerance_y)) and 
                                 (box_centroid[0] >= line_xmin and box_centroid[0] <= line_xmax))

            # Verificar se já não colidiu para não contar duas vezes
            current_collision = (xmin, ymin, xmax, ymax)
            if is_collision:
                vehicle = Mark.__vehicle_manager.get_vehicle_by_position(xmin, ymin, xmax, ymax)
                if not self.__calc_process.check_collision(self.__last_collision, current_collision):
                    if not vehicle is None and not vehicle.check_collision_mark(self.__label):
                        vehicle.register_collision_mark(self.__label)
                        self.__collision_counter = self.__collision_counter + 1
                self.__last_collision = current_collision
            else:
                if self.__calc_process.check_collision(self.__last_collision, current_collision):
                    self.__last_collision = current_collision
            
        return

    def draw(self, image, boxes, scores, classes):
        """ Desenhar as marcações """
        if self.__line_definition is None:
            return

        if not type(self.__line_definition) is LineMark:
            return

        # Verificar a colisão dos objetos
        self.check_collision_object(image, boxes, scores, classes)

        thickness = 2
        text = "%s : %i" % (self.__label, self.collision_counter)

        if self.__line_definition.y1 == self.__line_definition.y2:
            text_position = (self.__line_definition.x1, self.__line_definition.y1 - 5)
        else:
            text_position = (self.__line_definition.x1 + 5, self.__line_definition.y1 + 10)

        cv2.putText(
            image,
            text, 
            text_position,
            cv2.FONT_HERSHEY_SIMPLEX,
            0.35,
            (0, 0, 0),
            1,
            cv2.LINE_AA
        )

        cv2.line(image,
            self.__line_definition.start_position,
            self.__line_definition.end_position,
            self.__line_definition.color,
            thickness,
            8)  # lineType
        
        return

class MarkFactory:
    """ Fábrica de marcações """

    def __new__(cls, *args, **kwargs):
        """ Singleton """
        if not hasattr(cls, '_instance'):
            cls._instance = super(MarkFactory, cls).__new__(cls, *args, **kwargs)
        return cls._instance
    
    def build(self, marks : list) -> list:
        """ Retornar a lista de marcações """
        f_tuple_of_int = lambda x : tuple() if x is None else tuple(int(_) for _ in x.split(","))
        r = list()
        if not marks is None and len(marks) > 0:
            r = [Mark(_.get('label'), 
                      _.get('description'), 
                      f_tuple_of_int(_.get('line').get('color')),
                      f_tuple_of_int(_.get('line').get('startPosition')),
                      f_tuple_of_int(_.get('line').get('endPosition'))) 
                 for _ in marks
                 if not _.get('label') is None and 
                    not _.get('description') is None and
                    not _.get('line') is None and
                    not _.get('line').get('color') is None and
                    not _.get('line').get('startPosition') is None and
                    not _.get('line').get('endPosition') is None and 
                    len(f_tuple_of_int(_.get('line').get('color'))) == 3 and
                    len(f_tuple_of_int(_.get('line').get('startPosition'))) == 2 and 
                    len(f_tuple_of_int(_.get('line').get('endPosition'))) == 2]
        return r
