import math
import scipy.stats
import numpy as np

class CalcProcess:
    """ Classe responsável pelas funções de cálculo comuns da aplicação """

    def __new__(cls, *args, **kwargs):
        """ Singleton """
        if not hasattr(cls, '_instance'):
            cls._instance = super(CalcProcess, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    # Métodos estáticos
    def get_centroid(self, x1, y1, x2, y2) -> tuple:
        """ Calcular o centroid de um objeto """
        return (x1 + ((x2 - x1) / 2), y1 + ((y2 - y1) / 2))

    def check_collision(self, object_one : tuple, object_two : tuple) -> bool:
        """ Retornar verdadeiro se ocorreu uma colisão do objeto atual com o objeto anterior """
        if object_one is None or object_two is None:
            return False
            
        if len(object_one) != len(object_two) or len(object_one) != 4:
            return False

        xmin, ymin, xmax, ymax = object_one
        w = (xmax - xmin)
        h = (ymax - ymin)

        r_xmin, r_ymin, r_xmax, r_ymax = object_two
        r_w = (r_xmax - r_xmin)
        r_h = (r_ymax - r_ymin)

        return ((xmin + w > r_xmin) and (xmin < r_xmin + r_w) and  (ymin + h > r_ymin) and (ymin < r_ymin + r_h))

    def calculate_intersection_area(self, r1 : tuple, r2 : tuple) -> int:
        """ Calcular a área de intersecção entre dois retângulos """
        xmin = min(r1[0], r2[0])
        xmax = max(r1[2], r2[2])
        ymin = min(r1[1], r2[1])
        ymax = max(r1[3], r2[3])
        return sum([1
                    for x in range(xmin, xmax + 1, 1)
                    for y in range(ymin, ymax + 1, 1)
                    if x >= r1[0] and x <= r1[2] and y >= r1[1] and y <= r1[3]
                    if x >= r2[0] and x <= r2[2] and y >= r2[1] and y <= r2[3]])

    def calculate_eucledian_distance(self, x1, y1, x2, y2) -> float:
        """ Calcular a distância euclediana entre dois pontos x,y """
        return math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))

    def calculate_interval_norm(self, values : list) -> tuple:
        """ Calcular o intervalor de confiança """
        mean_value = np.mean(values)
        std_value = np.std(values)
        return scipy.stats.norm.interval(0.95, loc=mean_value, scale=std_value)