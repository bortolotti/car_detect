from enum import Enum
from detect.media_base import MediaBase
from detect.media_video import MediaVideo
from detect.media_image import MediaImage
from config.settings import Settings

class MediaType(Enum):
    """
    Tipos de mídias
    """
    VIDEO = 1,
    IMAGE = 2

class MediaFactory:
    """
    Classe responsável pela criação de instâncias de Mídias
    """
    def __init__(self, media_type : MediaType):
        """ Construtor padrão """
        self.__media_type = media_type

    def get_media(self) -> MediaBase:
        """ Retornar o objeto que encapsula as funcionalidades do Opencv + Tensorflow """
        r = None        
        if self.__media_type == MediaType.VIDEO:
            r = MediaVideo()
        elif self.__media_type == MediaType.IMAGE:
            r = MediaImage()
        else:
            raise Exception("Invalid media type")
        return r