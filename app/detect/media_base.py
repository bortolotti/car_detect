from abc import ABC, abstractmethod

class MediaBase(ABC):
    """ 
    Classe padrão que encapsula as funcionalidades do OpenCV + Tensorflow.
    """
    def __init__(self):
        """ Construtor padrão """
        super().__init__()

    @abstractmethod
    def process(self):
        pass

