import cv2
import os
from detect.media_base import MediaBase
from config.settings import Settings
from network.model import ModelNetwork
from detect.mark import MarkFactory
from detect.mark import Mark
from detect.vehicle import VehicleManager
from detect.vehicle import Vehicle
from stabilize.video import Stabilizer
from detect.report import ReportWriter

class MediaVideo(MediaBase):
    """ Classe responsável pelo processamento do vídeo """

    __stabilizer = Stabilizer()

    def process(self):
        """ Processar o vídeo """
        settings = Settings()        
        input_file = settings.get_media_input_file_path
        output_file = settings.get_media_output_file_path
        is_stabilize = settings.get_media_stabilize
        show_detect = settings.get_show_detection

        if input_file is None:
            raise Exception("Input file is required")

        if not os.path.isfile(input_file):
            raise Exception("Input file %s not found" % input_file)

        if not output_file is None and os.path.isfile(output_file):
            raise Exception("Output file %s already exists" % output_file)

        if is_stabilize:
            """ Efetuar a estabilização do vídeo """
            input_file = MediaVideo.__stabilizer.run(input_file)

        vehicle_manager = VehicleManager()
        mark_factory = MarkFactory()
        marks = mark_factory.build(settings.get_marks)

        network = ModelNetwork()

        # Abrir o vídeo
        try:
            input_video = cv2.VideoCapture(input_file)
            frame_width = int(input_video.get(3))
            frame_height = int(input_video.get(4))
            output_video = None

            if not output_file is None:
                output_video = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'mp4v'), 10, (frame_width,frame_height))

            frame_counter = 0

            while (input_video.isOpened()):

                frame_counter += 1

                _, frame = input_video.read()

                show_detect_in_frame = (output_file is None or show_detect)

                # Chamar a rede treinada para detecção
                (boxes, scores, classes, num) = network.detect(frame, show_detect_in_frame)

                # TODO: Rastreamento dos veículos
                vehicle_manager.track_vehicles(frame_counter, frame, boxes, scores, classes)
                vehicle_manager.draw(frame, boxes)

                # Desenhar as marcações e checar as colisões
                if not marks is None:
                    for mark in marks:
                        mark.draw(frame, boxes, scores, classes)

                if not output_video is None:
                    output_video.write(frame)

                if show_detect_in_frame:
                    cv2.imshow('Detector', frame)

                # Detectar se uma tecla foi pressionada
                if cv2.waitKey(1) == ord('q'):
                    break
        
        except Exception as error:
            raise error

        finally:
            released = False
            if not input_video is None:
                input_video.release()
                released = True

            if not output_video is None:
                output_video.release()
                released = True

            if released:
                cv2.destroyAllWindows()

        
        # Gerar os arquivos de contagem e gráficos
        report = ReportWriter(marks, vehicle_manager.get_vehicles())
        report.run()
        
        return 