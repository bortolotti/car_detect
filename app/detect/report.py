import pandas as pd
import os

from config.settings import Settings
from detect.vehicle import Vehicle
from detect.mark import Mark

class ReportWriter:
    """ Classe responsável pela geração dos relatórios de contagem """
    
    __settings = Settings()

    def __init__(self, marks, vehicles):
        """ Construtor padrão """
        self.__marks = marks
        self.__vehicles = vehicles

    def __generate_general_counter(self):
        """ Gerar a contagem geral dos veículos """
        output_file = ReportWriter.__settings.get_report_output_general_counter
        
        if output_file is None or output_file.strip() == "":
            return

        if os.path.exists(output_file):
            os.remove(output_file)

        if self.__marks is None or len(self.__marks) == 0:
            return

        frame = {
            "label" : [_.label for _ in self.__marks],
            "description" : [_.description for _ in self.__marks],
            "counter" : [_.collision_counter for _ in self.__marks]
        }

        df = pd.DataFrame(frame)
        df.to_csv(output_file, header=True, index=False)
        del df

        return

    def __generate_stream_counter(self):
        """ Gerar a contagem por streaming dos veículos """
        output_file = ReportWriter.__settings.get_report_output_stream_counter
        
        if output_file is None or output_file.strip() == "":
            return

        if os.path.exists(output_file):
            os.remove(output_file)

        if self.__marks is None or len(self.__marks) == 0:
            return

        if self.__vehicles is None or len(self.__vehicles) == 0:
            return

        stream_list = [{ "from" : _from.label, "to" : _to.label, "counter" : 0 }
                       for _from in self.__marks
                       for _to in self.__marks
                       if _from.label != _to.label ]

        for stream in stream_list:

            stream_vehicle_list = [_ 
                                   for _ in self.__vehicles
                                   if len(_.collision_marks) == 2
                                   if _.collision_marks[0] == stream["from"]
                                   if _.collision_marks[1] == stream["to"]]

            for _ in stream_vehicle_list:
                stream["counter"] = stream["counter"] + 1

        df = pd.DataFrame(stream_list)
        df.to_csv(output_file, header=True, index=False)
        del df        

        return

    def run(self):
        self.__generate_general_counter()
        self.__generate_stream_counter()

