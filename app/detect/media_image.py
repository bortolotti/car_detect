import cv2
import os
from detect.media_base import MediaBase
from config.settings import Settings
from network.model import ModelNetwork

class MediaImage(MediaBase):
    """ Classe responsável pelo processamento de uma imagem """
    
    def process(self):
        """ Processar o vídeo """
        settings = Settings()        
        input_file = settings.get_media_input_file_path
        output_file = settings.get_media_output_file_path
        show_detect = settings.get_show_detection

        if input_file is None:
            raise Exception("Input file is required")

        if not os.path.isfile(input_file):
            raise Exception("Input file %s not found" % input_file)

        if not output_file is None and os.path.isfile(output_file):
            raise Exception("Output file %s already exists" % output_file)

        network = ModelNetwork()

        # Abrir o vídeo
        try:
            input_image = cv2.imread(input_file)           
            show_detect_in_frame = (output_file is None or show_detect)

            # Chamar a rede treinada para detecção
            (boxes, scores, classes, num) = network.detect(input_image, show_detect_in_frame)

            print("boxes->", boxes)
            print("scores->", scores)
            print("classes->", classes)
            print("num->", num)

            # TODO: Desenhar as marcações de colisões
            # TODO: Desenhar quadro do resumo das detecções
            # TODO: Gerar o arquivo de contagem

            if not output_file is None:
                cv2.imwrite(output_file)

            if show_detect_in_frame:
                cv2.imshow('Detector', frame)

            cv2.waitKey(0)                
        
        except Exception as error:
            raise error

        finally:
            cv2.destroyAllWindows()
        
        return 