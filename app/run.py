# ================================================================================
# Programa principal -> car_detect
# ================================================================================

from config.settings import Settings
from detect.media_factory import MediaType
from detect.media_factory import MediaFactory

# import argparse
# parser = argparse.ArgumentParser(description='Run the car_detect')
# parser.add_argument('-s', action='store_true', required=False, dest='setting_file', help='Full path of the settings.json configuration file')
# arguments = parser.parse_args()

def main():

    print("Starting car_detect ...")

    print("Loading setting file ...")

    # Instânciar o objeto responsável pela leitura dos parâmetros
    settings = Settings()
    
    # Identificar o tipo de mídia
    media_type = settings.get_media_type
    media_type_object = None

    if media_type == "VIDEO":
        media_type_object = MediaType.VIDEO
    elif media_type == "IMAGE":
        media_type_object = MediaType.IMAGE
    else:
        raise Exception("Invalid media type in configuration file")

    media_factory = MediaFactory(media_type_object)
    media = media_factory.get_media()

    print("Processing the %s ..." % media_type_object)
    media.process()

    return

if __name__ == "__main__":
    main()