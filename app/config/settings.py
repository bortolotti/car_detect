﻿import json
import os

class Settings:
    """ 
    Classe responsável pela leitura do arquivo de configuração da aplicação
    """

    DEFAULT_CONFIG_FILE = "settings.json"

    def __new__(cls, *args, **kwargs):
        """ Singleton """
        if not hasattr(cls, '_instance'):
            cls._instance = super(Settings, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        """ Construtor padrão """
        self.__report_output_general_counter = ""
        self.__report_output_stream_counter = ""
        self.__media_type = ""
        self.__media_input_file_path = ""
        self.__media_output_file_path = ""
        self.__media_stabilize = False
        self.__network_checkpoint_file_path = ""
        self.__network_label_file_path = ""
        self.__network_number_classes = 0
        self.__network_min_score_thresh = 0.6
        self.__show_detection = False
        self.__marks = list()
        self.__read_config_file()

    @property
    def get_report_output_general_counter(self) -> str:
        return self.__report_output_general_counter
    
    @property
    def get_report_output_stream_counter(self) -> str:
        return self.__report_output_stream_counter

    @property
    def get_network_min_score_thresh(self) -> float:
        return self.__network_min_score_thresh

    @property
    def get_marks(self) -> list:
        return self.__marks

    @property
    def get_media_type(self):
        return self.__media_type

    @property
    def get_media_input_file_path(self):
        return self.__media_input_file_path

    @property
    def get_media_output_file_path(self):
        return self.__media_output_file_path

    @property
    def get_media_stabilize(self):
        return self.__media_stabilize

    @property
    def get_network_checkpoint_file_path(self):
        return self.__network_checkpoint_file_path

    @property
    def get_network_label_file_path(self):
        return self.__network_label_file_path

    @property
    def get_network_number_classes(self):
        return self.__network_number_classes

    @property
    def get_show_detection(self):
        return self.__show_detection

    def __read_config_file(self):
        """ Efetuar a leitura e carga das informações do arquivo de configuração da aplicação """
        path = os.getcwd()
        config_file_path = os.path.join(path, Settings.DEFAULT_CONFIG_FILE)
        if not os.path.isfile(config_file_path):
            raise Exception("Settings file %s not found" % config_file_path)
        settings = {}
        with open(config_file_path, 'r', encoding='utf8') as fp:
             settings = json.load(fp)
        if settings:
            if settings.get('report'):
                if settings['report'].get('outputGeneralCounter'):
                    self.__report_output_general_counter = str(settings['report']['outputGeneralCounter'])
                if settings['report'].get('outputStreamCounter'):
                    self.__report_output_stream_counter = str(settings['report']['outputStreamCounter'])
            
            if settings.get('media'):
                if settings['media'].get('type'):
                    self.__media_type = str(settings['media']['type'])
                if settings['media'].get('inputFilePath'):
                    self.__media_input_file_path = str(settings['media']['inputFilePath'])
                if settings['media'].get('outputFilePath'):
                    self.__media_output_file_path = str(settings['media']['outputFilePath'])
                if settings['media'].get('showDetection'):
                    self.__show_detection = True if str(settings['media']['showDetection']).upper() == "TRUE" else False
                if settings['media'].get('stabilize'):
                    self.__media_stabilize = True if str(settings['media']['stabilize']).upper() == "TRUE" else False

            if settings.get('network'):
                if settings['network'].get('checkpointFilePath'):
                    self.__network_checkpoint_file_path = str(settings['network']['checkpointFilePath'])
                if settings['network'].get('labelFilePath'):
                    self.__network_label_file_path = str(settings['network']['labelFilePath'])
                if settings['network'].get('numberClasses'):
                    try:
                        self.__network_number_classes = int(settings['network']['numberClasses'])
                    except ValueError:
                        self.__network_number_classes = -1
                if settings['network'].get('minScoreThresh'):
                    try:
                        self.__network_min_score_thresh = float(settings['network']['minScoreThresh'])
                    except ValueError:
                        self.__network_min_score_thresh = 0.6

            if settings.get('marks'):
                self.__marks = settings.get('marks')

        return settings