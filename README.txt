Treinar o modelo:

- Colocar as imagens do dataset na subpasta dataset/images/
- Colocar as labels do dataset (.txt) na subpasta dataset/labels/
- Executar o script run_prepare_dataset.py para separar e gerar arquivos .CSV de treinamento e testes. 
  Ele irá gerar dois arquivos na pasta dataset: test.csv e train.csv
- Converter os arquivos .csv para binário entendível pelo tensorflow, ele irá gerar 2 arquivos na pasta dataset: train.record e test.record. 
  Basta executar o script run_convert_csv_to_tfrecord.py
    python .\run_convert_csv_to_tfrecord.py --csv_input=dataset/train.csv --image_dir=dataset/images --output_path=training/train.record
    python .\run_convert_csv_to_tfrecord.py --csv_input=dataset/test.csv --image_dir=dataset/images --output_path=training/test.record

- Baixar o modelo coco e colocar dentro da pasta training/network (Se necessário)
- Editar o arquivo faster_rcnn_inception_v2_coco.config e ajustar os parâmetros dos diretórios
- Efetuar o treinamento, para isso executar o script train.py que está localizado dentro da pasta ..\Tensorflow\models\research\object_detection\legacy\train.py.
   python  ..\Tensorflow\models\research\object_detection\legacy\train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_inception_v2_coco.config
- Executar o tensorboard para acompanhar o treinamento:
    tensorboard --logdir=training
- Exportar o gráfico de inferência gerado pelo treinamento:
    python ..\Tensorflow\models\research\object_detection\export_inference_graph.py --input_type image_tensor --pipeline_config_path training/faster_rcnn_inception_v2_coco.config --trained_checkpoint_prefix training/model.ckpt-76067 --output_directory training/inference_graph

Testar o modelo:


https://github.com/aniskoubaa/car_detection_yolo_faster_rcnn_uvsc2019
