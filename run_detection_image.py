# ==================================================================================================
# Detecção de objetos com base no modelo.
# Autor: Carlos V. Bortolotti
# ==================================================================================================

import os
import cv2
import numpy as np
import tensorflow as tf
import sys

sys.path.append("../Tensorflow/models/research/object_detection")

from utils import label_map_util
from utils import visualization_utils as vis_util

# Definição das constantes
CWD_PATH = os.getcwd() # Diretório corrente
TRAINING_NAME = 'training' # Subdiretório base que contém os arquivos TF de treinamento.
ASSETS_NAME = 'assets' # Subdiretório base que contém os assets utilizados para testar.
MODEL_NAME = 'inference_graph' # Nome do subdiretório que contém os arquivos referente ao gráfico de inferência.
IMAGE_NAME = '2_co.png' # Nome da imagem
PATH_TO_CKPT = os.path.join(CWD_PATH, TRAINING_NAME, MODEL_NAME, 'frozen_inference_graph.pb') # Caminho relativo ao arquivo do gráfico de inferência
PATH_TO_LABELS = os.path.join(CWD_PATH, TRAINING_NAME, 'mapa.pbtxt') # Caminho relativo aos labels das classes
PATH_TO_IMAGE = os.path.join(CWD_PATH, ASSETS_NAME, IMAGE_NAME) # Caminho relativo ao arquivo a ser verificado
NUM_CLASSES = 5 # Número de classes

# Definição de variáveis
label_map = label_map_util.load_labelmap(PATH_TO_LABELS) # Carregar o mapa das labels das classes
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Carregando o modelo treinado
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')
    sess = tf.Session(graph=detection_graph)

# Entrada -> Imagem
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Saída -> Detecção das caixas, score e classe
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Carregar a imagem
image = cv2.imread(PATH_TO_IMAGE)
image_expanded = np.expand_dims(image, axis=0)

# Efetuar a detecção
(boxes, scores, classes, num) = sess.run(
    [detection_boxes, detection_scores, detection_classes, num_detections],
    feed_dict={image_tensor: image_expanded})

# Desenhar o resultado
vis_util.visualize_boxes_and_labels_on_image_array(
    image,
    np.squeeze(boxes),
    np.squeeze(classes).astype(np.int32),
    np.squeeze(scores),
    category_index,
    use_normalized_coordinates=True,
    line_thickness=8,
    min_score_thresh=0.60)

# Mostrar a imagem
cv2.imshow('Object detector', image)

# Detectar se uma tecla foi pressionada
cv2.waitKey(0)

# Destruir todas as janelas
cv2.destroyAllWindows()